import { calculateBonuses } from './bonus-system'

describe('Bonus system tests', () => {
    it("Standard program", () => {
        expect(calculateBonuses("Standard", 1000)).toEqual(0.05 * 1);
        expect(calculateBonuses("Standard", 10000)).toEqual(0.05 * 1.5);
        expect(calculateBonuses("Standard", 50000)).toEqual(0.05 * 2);
        expect(calculateBonuses("Standard", 100000)).toEqual(0.05 * 2.5);
    });

    it("Premium program", () => {
        expect(calculateBonuses("Premium", 1000)).toEqual(0.1 * 1);
        expect(calculateBonuses("Premium", 10000)).toEqual(0.1 * 1.5);
        expect(calculateBonuses("Premium", 50000)).toEqual(0.1 * 2);
        expect(calculateBonuses("Premium", 100000)).toEqual(0.1 * 2.5);
    });

    it("Diamond program", () => {
        expect(calculateBonuses("Diamond", 1000)).toEqual(0.2 * 1);
        expect(calculateBonuses("Diamond", 10000)).toEqual(0.2 * 1.5);
        expect(calculateBonuses("Diamond", 50000)).toEqual(0.2 * 2);
        expect(calculateBonuses("Diamond", 100000)).toEqual(0.2 * 2.5);
    });

    it("Other program", () => {
        expect(calculateBonuses("Other", 1000)).toEqual(0);
        expect(calculateBonuses("Other", 10000)).toEqual(0);
        expect(calculateBonuses("Other", 50000)).toEqual(0);
        expect(calculateBonuses("Other", 100000)).toEqual(0);
    });
});
